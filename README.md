# Mail Server Setup for lionsden.local

This project sets up a mail server for the `lionsden.local` domain using [Mailu](https://mailu.io) and includes a webmail interface powered by [RainLoop](https://www.rainloop.net/). The project is licensed under the MIT License.

## Requirements

- Docker
- Docker Compose

## Installation

1. Clone this repository:
git clone https://github.com/yourusername/lionsden-mailserver.git
cd lionsden-mailserver
 
2. Create a `mailu.env` file using the provided `mailu.env.example` as a template. Replace the placeholders with your actual domain, timezone, and desired passwords.

3. Initialize the Docker Swarm:

docker swarm init
 
4. Deploy the stack using Docker Swarm:

docker stack deploy -c docker-compose.yml lionsden_mail
 

5. Configure your DNS settings according to the [Mailu documentation](https://mailu.io/1.8/dns.html).

6. (Optional) Obtain SSL certificates for your domain using [Let's Encrypt](https://letsencrypt.org/) or another certificate authority, and configure the mail server to use them. Update the `TLS_FLAVOR` setting in the `mailu.env` file accordingly.

## Usage

1. Access the Mailu admin interface at `https://mail.lionsden.local/admin`. Log in using the admin email `admin@lionsden.local` and the password you set in the `mailu.env` file.

2. Create new email accounts by clicking on "New user" in the admin interface and entering the desired email address and password.

3. Access the RainLoop webmail interface at `https://mail.lionsden.local/webmail` and log in using the email accounts created in step 2.

## License

MIT License

Copyright (c) 2023 Your Name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
